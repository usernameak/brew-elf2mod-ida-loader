#include "idaldr.h"

#include <stdint.h>
#include <stdlib.h>

#pragma pack(push, 1)
struct ELF2ModHeader {
	uint32_t stub_0;
	uint32_t stub_1;
	char magic[4];

	uint32_t unk_0;
	uint32_t unk_1;
	uint16_t unk_2;
	uint8_t unk_3;
	uint8_t status;
	uint32_t text_addr;
	uint32_t text_size;
	uint32_t reloc_table_offset;
	uint32_t reloc_table_size;
	uint32_t reloc_func_offset;
	uint32_t unk_4;
	uint32_t unk_5;
	uint32_t text_size2;
	uint32_t unk_6;
	uint32_t unk_7;
};
#pragma pack(pop)

static_assert(sizeof(ELF2ModHeader) == 0x40, "bad elf2mod header size");

static bool check_new_elf2mod(linput_t *li) {
	qlseek(li, 8);
	char magic[4];
	if (qlread(li, &magic, sizeof(magic)) != sizeof(magic)) {
		return false;
	}
	if (memcmp(magic, "BREW", sizeof(magic)) == 0) {
		return true;
	}
	return false;
}

static int idaapi accept_file(qstring* fileformatname, qstring* processor, linput_t* li, const char* filename) {
	if (check_new_elf2mod(li)) {
		*fileformatname = "BREW elf2mod Binary";
		*processor = "ARM";
		return true;
	}

	qlseek(li, 0);
	unsigned char jump[4];
	const unsigned char expectedJump[4] {0x03, 0x00, 0x00, 0xEA};
	if (qlread(li, &jump, sizeof(jump)) != sizeof(jump)) {
		return false;
	}
	if (memcmp(jump, expectedJump, sizeof(jump)) == 0) {
		*fileformatname = "BREW elf2mod(legacy) Binary";
		*processor = "ARM";
		return true;
	}

	return false;
}

#define BREW_LOAD_BASE_ADDR 0x400000

void idaapi load_file(linput_t* li, ushort /*neflag*/, const char* /*fileformatname*/) {
	set_processor_type("ARM", SETPROC_LOADER);

	if (check_new_elf2mod(li)) {
		uint64 fsize = qlsize(li);
		qlseek(li, 0);

		ELF2ModHeader header;
		qlread(li, &header, sizeof(header));

		file2base(
			li,
			header.text_addr,
			BREW_LOAD_BASE_ADDR,
			BREW_LOAD_BASE_ADDR + fsize - header.text_addr,
			1);

		segment_t s1;
		s1.sel = 0x0;
		s1.start_ea = BREW_LOAD_BASE_ADDR;
		s1.end_ea = BREW_LOAD_BASE_ADDR + header.text_size;
		s1.align = saRelDble;
		s1.comb = scPub;
		s1.bitness = 1; // 32-bit

		if (!add_segm_ex(&s1, NAME_CODE, CLASS_CODE, ADDSEG_NOSREG))
			loader_failure();

		segment_t s2;
		s2.sel = 0x1;
		s2.start_ea = BREW_LOAD_BASE_ADDR + header.text_size;
		s2.end_ea = BREW_LOAD_BASE_ADDR + fsize - header.text_addr;
		s2.align = saRelDble;
		s2.comb = scPub;
		s2.bitness = 1; // 32-bit

		if (!add_segm_ex(&s2, NAME_DATA, CLASS_DATA, ADDSEG_NOSREG))
			loader_failure();

		segment_t s3;
		s3.sel = 0x2;
		s3.start_ea = BREW_LOAD_BASE_ADDR - 8;
		s3.end_ea = BREW_LOAD_BASE_ADDR;
		s3.align = saRelDble;
		s3.comb = scPub;
		s3.bitness = 1; // 32-bit

		if (!add_segm_ex(&s3, ".brew_std", CLASS_DATA, ADDSEG_NOSREG))
			loader_failure();

		set_selector(0, 0);

		uint64 reloc_table_addr = header.text_addr + header.text_size + header.reloc_table_offset;
		qlseek(li, reloc_table_addr);

		uint64 text_base = BREW_LOAD_BASE_ADDR;
		uint64 data_base = text_base + header.text_size;

		while (true) {
			uint32_t record;
			if (qlread(li, &record, sizeof(record)) != sizeof(record)) {
				loader_failure("invalid relocation table");
			}
			if (!record) break;
			if (record & 0x3F) {
				// skip
				uint32_t skip_offset = 4 * (record >> 8);
				if (record & 0x20) {
					skip_offset *= 2;
				}
				qlseek(li, skip_offset, SEEK_CUR);
			}
			else {
				uint32_t num_relocs = record >> 8;

				uint64 base_addr = text_base;
				if (record & 0x80) {
					base_addr = data_base;
				}

				for (uint32_t i = 0; i < num_relocs; i++) {
					if (qlread(li, &record, sizeof(record)) != sizeof(record)) {
						loader_failure("invalid relocation table");
					}
					
					ea_t ea = base_addr + record;

					patch_dword(ea, get_wide_dword(ea) + BREW_LOAD_BASE_ADDR);

					fixup_data_t fd(FIXUP_OFF32);
					fd.set_sel(&s1);
					fd.set(ea);
				}
			}
		}
	} else {
		uint64 fsize = qlsize(li);
		qlseek(li, 4);

		uint32 reloc_start, reloc_end;
		qlread(li, &reloc_start, sizeof(uint32));
		qlread(li, &reloc_end, sizeof(uint32));

		uint32 text_addr = 0x9C;

		file2base(
			li,
			text_addr,
			BREW_LOAD_BASE_ADDR,
			BREW_LOAD_BASE_ADDR + fsize - text_addr,
			1);
		
		segment_t s1;
		s1.sel = 0x0;
		s1.start_ea = BREW_LOAD_BASE_ADDR;
		s1.end_ea = BREW_LOAD_BASE_ADDR + qlsize(li) - text_addr;
		s1.align = saRelDble;
		s1.comb = scPub;
		s1.bitness = 1; // 32-bit

		if (!add_segm_ex(&s1, NAME_CODE, CLASS_CODE, ADDSEG_NOSREG))
			loader_failure();

		segment_t s3;
		s3.sel = 0x2;
		s3.start_ea = BREW_LOAD_BASE_ADDR - 8;
		s3.end_ea = BREW_LOAD_BASE_ADDR;
		s3.align = saRelDble;
		s3.comb = scPub;
		s3.bitness = 1; // 32-bit

		if (!add_segm_ex(&s3, ".brew_std", CLASS_DATA, ADDSEG_NOSREG))
			loader_failure();

		set_selector(0, 0);

		uint64 reloc_table_addr = reloc_start + text_addr;
		qlseek(li, reloc_table_addr);
		for (uint32 i = 0; i < (reloc_end - reloc_start) / sizeof(uint32); i++) {
			uint32 record;
			if (qlread(li, &record, sizeof(record)) != sizeof(record)) {
				loader_failure("invalid relocation table");
			}

			ea_t ea = BREW_LOAD_BASE_ADDR + record;

			patch_dword(ea, get_wide_dword(ea) + BREW_LOAD_BASE_ADDR);
			
			fixup_data_t fd(FIXUP_OFF32);
			fd.set_sel(&s1);
			fd.set(ea);
		}
	}
	add_entry(0, BREW_LOAD_BASE_ADDR, "AEEMod_Load", true, 0);
}

loader_t LDSC = {
  IDP_INTERFACE_VERSION,
  0,
  accept_file,
  load_file,
  NULL,
  NULL,
  NULL,
};
